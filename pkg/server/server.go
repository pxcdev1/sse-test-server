package server

import (
	"bitbucket.org/pxcdev1/sse-test-server/http/rest"
	root "bitbucket.org/pxcdev1/sse-test-server/pkg"
	"fmt"
	"github.com/go-playground/validator/v10"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"time"
)

type Server struct {
	router    *mux.Router
	config    *root.ServerConfig
	server    *http.Server
	logger    *log.Logger
	validator *validator.Validate
}

func NewServer(config *root.Config, logger *log.Logger, validator *validator.Validate) *Server {
	router := mux.NewRouter()
	handler := rest.NewHandler(logger, validator)
	handler.SetupRoutes(router)

	addr := fmt.Sprintf(":%s", config.Server.Port)
	srv := &http.Server{
		Addr:         addr,
		Handler:      router,
		TLSConfig:    config.Server.Tls,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
		IdleTimeout:  120 * time.Second,
		TLSNextProto: nil,
	}

	s := Server{
		router:    router,
		config:    config.Server,
		server:    srv,
		logger:    logger,
		validator: validator,
	}

	return &s
}

func (s *Server) Start() {
	s.logger.Printf("Server started on port %s\n", s.config.Port)
	err := s.server.ListenAndServe()
	if err != nil {
		panic(err)
	}
}
