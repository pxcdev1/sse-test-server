package root

type SsePayload struct {
	Topic string `validate:"required,uri"`
}
