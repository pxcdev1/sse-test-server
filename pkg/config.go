package root

import "crypto/tls"

type ServerConfig struct {
	Port string
	Tls  *tls.Config
}

type Config struct {
	Server *ServerConfig
}
