package main

import (
	root "bitbucket.org/pxcdev1/sse-test-server/pkg"
	"bitbucket.org/pxcdev1/sse-test-server/pkg/config"
	"bitbucket.org/pxcdev1/sse-test-server/pkg/server"
	"github.com/go-playground/validator/v10"
	"log"
	"os"
)

type App struct {
	server    *server.Server
	config    *root.Config
	logger    *log.Logger
	validator *validator.Validate
}

func (a *App) Initialize() {
	a.logger = log.New(os.Stdout, "pxcdev", log.LstdFlags|log.Lshortfile)
	a.logger.Printf("Logger initialized.\n")
	a.validator = validator.New()
	a.config = config.GetConfig()
	a.server = server.NewServer(a.config, a.logger, a.validator)
}

func (a *App) Run() {
	a.logger.Printf("App initialized and now starting...\n")
	a.server.Start()
}
