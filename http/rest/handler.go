package rest

import (
	root "bitbucket.org/pxcdev1/sse-test-server/pkg"
	"encoding/json"
	"github.com/go-playground/validator/v10"
	"github.com/gorilla/mux"
	"log"
	"net/http"
)

type Handler struct {
	logger    *log.Logger
	validator *validator.Validate
}

func NewHandler(logger *log.Logger, validator *validator.Validate) *Handler {
	return &Handler{logger: logger, validator: validator}
}

func (h Handler) SetupRoutes(router *mux.Router) {
	router.HandleFunc("/init-sse", h.initSse).Methods(http.MethodPost)
}

func (h Handler) initSse(w http.ResponseWriter, r *http.Request) {
	ssePayload := root.SsePayload{}
	json.NewDecoder(r.Body).Decode(&ssePayload)
	err := h.validator.Struct(ssePayload)
	if err != nil {
		validationErrors := err.(validator.ValidationErrors)
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusBadRequest)
		responseBody := map[string]string{"error": validationErrors.Error()}
		if err := json.NewEncoder(w).Encode(responseBody); err != nil {
		}
		return
	}

	// TODO: write custom http client -- Udemy course
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	// Simple JSON
	resp := make(map[string]string)
	resp["data"] = "yo"
	newJson, err := json.Marshal(resp)
	if err != nil {
		h.logger.Fatal("Marshalling error: %v", err)
	}
	w.Write(newJson)
	return
}
